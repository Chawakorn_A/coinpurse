package coinpurse;
/**
 * Abstact superclass of Coin,BankNote,Coupon that contain valuable and comparable.
 * @author Chawakorn Aougsuk
 *
 */
public abstract class AbstractValuable implements Valuable {

	/**
	 * value of the money type
	 */
	private double value=0;
	
	/**
	 * Construct and declare the value of the money.
	 * @param value is the value of the money
	 */
	public AbstractValuable(double value)
	{
		this.value = value;
	}
	
	/**
	 * get the value of the money.
	 * @return vaule of the money
	 */
	public double getValue()
	{
		return this.value;
	}
	
	/** 
     * compare the value of this money to the other. 
     * @param other a money to compare to this one
     * @return negative number when this value is lower than other , positive number when this value is greater than other
     */
	public int compareTo (Valuable other)
	{
	    	if(other == null)
	    		return -1;
			return (int)(this.getValue()-other.getValue());
	}
	
	/**
	 * 
     * check if this money is equal the 'value' of the other.
     * @param other a money to compare to this one
     * @return true if the value of both are the same
     *  
     */
	public boolean equals (Object obj)
	{
		if(obj == null)
    		return false;
    	if(obj.getClass()!=this.getClass())
    		return false;
    	return this.getValue() == ((Valuable)obj).getValue();
	}
}
