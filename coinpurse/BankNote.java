package coinpurse;
/**
 * A note with a monetary value.
 * You can't change the value of a note.
 * @author Chawakorn Aougsuk
 */
public class BankNote extends AbstractValuable implements Valuable{

   
  
    /**
     * serial number of the note
     */
    private int serial;
    
    private static int nextSerialNumber = 1000000;
    
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the bank note
     */
    public BankNote( double value ) {
        super(value);
        this.getNextSerialNumber();
    }

    
    public void getNextSerialNumber()
    {
    	this.serial = nextSerialNumber;
    	this.nextSerialNumber++;
    }
    
    /** 
     * Get the Value of this coin.
     * @return the value of this coin 
     */

    
    /** 
     * return String of this coin. 
     * @return String of the amount of coin in baht
     */
    public String toString ()
    {
    	return String.format("%.1f-Baht Banknote [%d]", this.getValue(),this.serial);
    }
}
