package coinpurse;

 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Chawakorn Aougsuk
 */
public class Coin extends AbstractValuable implements Valuable{

    private String currency = "";
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value,String currency ) {
        super(value);
        this.currency = currency;
    }

    
    /** 
     * return String of this coin. 
     * @return String of the amount of coin in baht
     */
    public String toString ()
    {
    	return String.format("%.1f-%s Coin", this.getValue(),currency);
    }
    
}
