package coinpurse;

/**
 * A coupon with a color that stand for monetary value.
 * You can't change the color of a coupon.
 * @author Chawakorn Aougsuk
 */
public class Coupon extends AbstractValuable implements Valuable {

	public enum CouponColor {

		RED("red",100),BLUE("blue",50),GREEN("green",20);
		
		private double value;
		private String color;
	    //Constructor which will initialize the enum
	    CouponColor(String color,double value)
	    {
	    	this.color = color;
	    	this.value = value;
	    }
	    
	    //method to return the direction set by the user which initializing the enum
	    public double value()
	    {
	      return value;
	    }
	    
	    public String color()
	    {
	    	return color;
	    }
	}
	
	/** Value of the coin */
    private double value;
    private String color;
    
    /** 
     * Constructor for a new coin. 
     * @param color is the color of coupon
     */
    public Coupon( CouponColor  color ) {
    	super ( color.value );
        this.color = color.color;
    }

    /**
     * Create a coupon given the string name of a color.
     * @param cname is string name for the coupon color
     */
    public Coupon(String cname) {
    	//System.out.print(CouponColor.valueOf(cname.toUpperCase()).value);
    	super (  CouponColor.valueOf(cname.toUpperCase()).value);
    	this.color =  CouponColor.valueOf(cname.toUpperCase()).color;
    
    }
    
    
    /**
     * Get the color of this coin.
     * @return color of this coupon
     */
    public String getColor()
    {
    	return this.color;
    }
    
    /** 
     * return String of this coin. 
     * @return String of the amount of coin in baht
     */
    public String toString ()
    {
    	return String.format("%s coupon", this.getColor());
    }
	
	
}
