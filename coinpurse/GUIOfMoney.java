package coinpurse;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * A class of GUI that will tell the value of the money in purse.
 * @author Chawakorn Aougsuk
 *
 */
public class GUIOfMoney implements Observer{

	/**
	 * A label of value of money.
	 */
	private JLabel money;
	
	/**
	 * Constructor that will call the init.
	 */
	public GUIOfMoney()
	{
		init();
	}
	
	/**
	 * A method that set up the GUI
	 */
	public void init()
	{
		JFrame frameMoneyAmount = new JFrame();
		frameMoneyAmount.setLayout(new GridLayout(1,1) );
		frameMoneyAmount.setVisible(true);
		money = new JLabel("Balance is : 0.00 Baht");
		frameMoneyAmount.add(money);
		frameMoneyAmount.pack();
	}
	
	/**
	 * A method that will update the label every time that purse call notifies.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse)
		{
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			String text = String.format("Balance is : %.2f Baht", balance);
			money.setText(text);
			
			
		}
		
	}
}

