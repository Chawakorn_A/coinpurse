package coinpurse;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * A GUI class that tell the amount of money inside the purse,also contain progress bar.
 * @author Chawakorn Aougsuk
 *
 */
public class GUIOfStatus implements Observer{

	/**
	 * The capacity of the purse
	 */
	private int capacity;
	
	/**
	 * 
	 */
	private JProgressBar progress;
	
	/**
	 * 
	 */
	private JLabel Status ;
	
	/**
	 * Constructor that will call the init.
	 * @param capacity is the size of the purse
	 */
	public GUIOfStatus(int capacity)
	{
		this.capacity = capacity;
		this.init();
	}
	
	/**
	 * A method that set up the GUI
	 */
	private void init()
	{
		
		
		JFrame frameStatus = new JFrame();
		Status = new JLabel("Empty",JLabel.CENTER);
		frameStatus.setLayout(new GridLayout (2,1) );
		frameStatus.add(Status);
		progress = new JProgressBar(0,this.capacity);
		progress.setValue(0);
		frameStatus.add(progress);
		frameStatus.setVisible(true);
		frameStatus.pack();
	}
	
	/**
	 * A method that will update the label every time that purse call notifies.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse)
		{
			Purse purse = (Purse)subject;
			int amountInPurse = purse.count();
			String statusString = "";
			if(amountInPurse == 0)
				statusString = "Empty";
			else if(purse.isFull() )
				statusString = "Full";
			else
				statusString = ""+amountInPurse;
			Status.setText(statusString);
			this.progress.setValue(amountInPurse);
		}
		if(info!=null) System.out.println( info );
		
	}

}
