package coinpurse;

import java.util.Scanner;

 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

	
    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	int capacity = 5;
    	Purse purse = new Purse(capacity);
    	purse.setRecursiveStrategy();
    	ConsoleDialog dialog = new ConsoleDialog(purse);
    	GUIOfStatus guiOfStatus = new GUIOfStatus(capacity);
    	GUIOfMoney guiOfMoney = new GUIOfMoney();
    	purse.addObserver(guiOfStatus);
    	purse.addObserver(guiOfMoney);
    	dialog.run();
    	
    	
        

    }
}
