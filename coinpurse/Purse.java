package coinpurse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
//TODO import ArrayList and Collections (for Collections.sort())


import java.util.Observable;

import strategies.GreedyWithdraw;
import strategies.RecursiveWithdraw;

/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Chawakorn Aougsuk
 */
public class Purse extends Observable{
    /** Collection of coins in the purse. */
	private List <Valuable> moneyList = new ArrayList <Valuable> ();
	
	/**
	 * parameter that store the strategy for purse to withdraw.
	 */
	private WithdrawStrategy strategy;
    
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of coins you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity = capacity;
    	this.setRecursiveStrategy();
    }

    /**
     * Count and return the number of coins in the purse.
     * This is the number of coins, not their value.
     * @return the number of coins in the purse
     */
    public int count() { return this.moneyList.size(); }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double balance = 0;
    	for(int i = 0;i<this.moneyList.size();i++)
    	{
    		balance+= this.moneyList.get(i).getValue();
    	}
    	
    	return balance; }

    
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
    public int getCapacity() { return this.capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
        return this.capacity <= this.count();
    }

 
    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless coins!
     * @param money is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Valuable money ) {
    	if ( isFull( ) ) return false;
        if (money.getValue()>0)
        {
        	int i = 0 ;
        	for( i = 0 ;this.moneyList.size()==0||i<this.moneyList.size();i++){
        		if(this.moneyList.size()==0){
        			this.moneyList.add(i,money);
        			break;
        		}
        		else{
        			if(this.moneyList.get(i).getValue()>money.getValue()){
        				this.moneyList.add(i,money);
        				break;
        			}
        		}
        	}
        	if(i==this.moneyList.size())
        		this.moneyList.add(money);
        	super.setChanged();
        	super.notifyObservers(this);
        	return true;
        }
        return false;
    }
    
    
   
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    
    /**
     * Set the strategy as greedy
     */
    public void setGreedyStrategy()
    {
    	this.strategy = new GreedyWithdraw();
    }
    
    /**
     * 
     */
    public void setRecursiveStrategy()
    {
    	this.strategy = new RecursiveWithdraw();
    }
    
    public Valuable[] withdraw( double amount ) {
        
    	
    	List<Valuable> remove = strategy.withdraw(amount , this.moneyList);
    	
    	
		// remove coins from the purse
    	if(remove == null)
    	{

        	super.setChanged();
        	super.notifyObservers("withdraw fail");
    		return null;
    	}
		for(Valuable c : remove) {
			moneyList.remove(c);
			}
		
		Valuable[] coins = remove.toArray( new Valuable[remove.size()] );

    	super.setChanged();
    	super.notifyObservers(this);
        return coins;
	}
  
    /**
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     * @return temp String of the amount of of money 
     */
    public String toString() {
    	String temp = String.format("%d coins with value of %.1f\n",this.moneyList.size(),this.getBalance());
    	for(int i =0;i<this.moneyList.size();i++){
    		temp += this.moneyList.get(i).toString()+"\n"; 
    	}
    	return temp;
    }

}
