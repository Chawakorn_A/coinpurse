package coinpurse;

import java.util.List;

/**
 * An interface for the withdraw strategy that we are going to write for purse.
 * @author Chawakorn Aougsuk
 *
 */
public interface WithdrawStrategy {

	public List<Valuable> withdraw(double amount , List<Valuable> valuables);
}
