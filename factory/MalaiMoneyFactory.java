package factory;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;

/**
 * A MoneyFactory class forMalaysia-Ringgit money.
 * @author Chawakorn Aougsuk
 *
 */
public class MalaiMoneyFactory extends MoneyFactory{

	@Override
	public Valuable createMoney(double value) {
		Valuable money = null;
		if(value==0.05||value==0.1||value==0.2||value==0.5) money = new Coin(value,"Sen");

        else if(value==1||value==2||value==5||value==10||value==20||value==50||value==100) money = new Coin(value,"Ringgit");

		if(money == null) throw new IllegalArgumentException();
		return money ;
	}

	@Override
	public String getCurrency() {
		// TODO Auto-generated method stub
		return "Ringggit";
	}

}
