package factory;

import java.util.ResourceBundle;

import coinpurse.Valuable;

/**
 * An abstract class of the factory pattern.
 * @author Chawakorn Aougsuk
 *
 */
public abstract class MoneyFactory {

	private static MoneyFactory INSTANCE = null;

	/**
	 * For setting the factory inside the code
	 * @param moneyFactory a String with the name of factory you want
	 */
	public static void setMoneyFactory( String moneyFactory) {
		try {
			INSTANCE = (MoneyFactory) Class.forName(moneyFactory).newInstance();
		} catch (Exception e) {
			System.out.println("Error setting factory");
		}
	}

	/**
	 * Get the instance of the factory.
	 * @return MoneyFactory that's used
	 */
	public static MoneyFactory getInstance() {
		if (INSTANCE==null) {
			try {
				// create the resoursebundle
				ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
				// get the value of yhe "moneyfactory"
				String className =  bundle.getString( "moneyfactory" ); 
				// if no property then use a default value
				if( className.equals("") ) className = "factory.ThaiMoneyFactory";
				INSTANCE = (MoneyFactory) Class.forName(className).newInstance();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		return INSTANCE;
	}

	/**
	 * A method that create the Valuable money.
	 * @param value double of value of money that we want to create money
	 * @return Valuable money
	 */
	public abstract Valuable createMoney(double value);

	public Valuable createMoney(String value){
		return null;

	}
	/**
	 * A method that will get the currency of the current factory.
	 * @return String of currency
	 */
	public abstract String getCurrency();

}
