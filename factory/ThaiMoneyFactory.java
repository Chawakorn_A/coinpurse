package factory;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;

/**
 * A MoneyFactory class for Thai-Baht money.
 * @author Chawakorn Aougsuk
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{

	@Override
	public Valuable createMoney(double value) {
		Valuable money = null;
		if(value==1||value==2||value==5||value==10) money = new Coin(value,"Baht");

        else if(value==20||value==50||value==100||value==500||value==1000) money = new Coin(value,"Baht");        
		
		return money ;
	}

	@Override
	public String getCurrency() {
		// TODO Auto-generated method stub
		return "Baht";
	}

}
