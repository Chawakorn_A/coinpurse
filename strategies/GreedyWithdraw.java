package strategies;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.WithdrawStrategy;

/**
 * The strategy of withdrawal by using greedy algorithm.
 * @author Chawakorn Aougsuk
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {
	
	/**Withdrawal with greedy algorithm.
	 *@param amount amount is the money ,that you need
	 *@param moneyList is the list of money in your purse
	 *@return the list of money that possible ,if not null
	 */
	public List<Valuable> withdraw( double amount , List<Valuable> moneyList ) {
	        
	    	List<Valuable> templist  = new ArrayList<Valuable>();
	    	double total = 0;
	    	int index[] = new int[moneyList.size()];
	    	for(int i = moneyList.size()-1;i>=0;i--)
	    	{
				if (total + moneyList.get(i).getValue() <= amount) {

					templist.add(moneyList.get(i));
					total += moneyList.get(i).getValue();
				}
	    	}
	    	
	    	
			if ( amount !=total )
			{	// failed. Since you haven't actually remove
				// any coins from Purse yet, there is nothing
				// to put back.
				return null;
			}
			// remove coins from the purse
			return templist;
		}

}
