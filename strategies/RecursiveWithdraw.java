package strategies;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.WithdrawStrategy;

/**
 * The strategy of withdrawal by using recursion method for all possibility result.
 * @author Chawakorn Aougsuk
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy {
	
	/**
	 * A list of money form purse.
	 */
	private List<Valuable> moneylist;
	
	/**
	 * Size of the list from purse.
	 */
	private int size;
	
	/**
	 * the method that start the recursion.
	 * @param amount is the money ,that you need
	 * @param moneyList is the list of money in your purse
	 * @return List<Valuable> of the money you want to withdraw or null if not possible
	 */
	public List<Valuable> withdraw( double amount , List<Valuable> moneyList ) {
		this.moneylist = moneyList;
		this.size = moneylist.size();
		return this.getMoneyList(amount , 0);
	}
	
	/**
	 * recursion part
	 * @param amount is the amount of money left that you need for withdrawal.
	 * @param position is the index of list that we're at
	 * @return the list of money that possible
	 */
	private List<Valuable> getMoneyList (double amount , int position)
	{
		if(position > size-1)
		{
			return null;
		}
		
		if(amount<0)
		{
			return null;
		}
		
		if( 0 == amount - getValue(position) )
		{
			List<Valuable> list = new ArrayList();
			list.add(this.moneylist.get(position) );
			return list;
			
		}
		
		List<Valuable> temp = getMoneyList( amount-getValue(position) , position+1 );
		
		if(temp != null)
			temp.add(moneylist.get(position) );
		else
			temp = getMoneyList(amount , position+1);
			
		
		return temp;
		
	}
	
	/**
	 * A method to easily get the value of the money
	 * @param i is the index of the moneylist you want
	 * @return the double that is the value of money
	 */
	private double getValue(int i)
	{
		return this.moneylist.get(i).getValue();
	}
}
